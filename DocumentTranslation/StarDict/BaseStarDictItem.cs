﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentTranslation.StarDict
{
    public class BaseStarDictItem
    {
        protected string encoding;
        protected string dictionaryFile;
        protected long realFileSize;
        public BaseStarDictItem(string pathToDict, string exp) 
        {
            // Определяем переменную с кодировкой
            encoding = "utf-8";

            // Получаем полный путь до файла
            dictionaryFile = PathToFileInDirByExp(pathToDict, exp);
		
		    // Получаем размер файла
            realFileSize = new FileInfo(dictionaryFile).Length;
        }

        /// <summary>
        /// Метод ищет в папке path первый попапвшийся файл с расширением exp
        /// </summary>
        /// <param name="path"></param>
        /// <param name="exp"></param>
        /// <returns></returns>
        protected string PathToFileInDirByExp(string path, string exp) 
        {
            if (!Directory.Exists(path))
			    throw new Exception(string.Format($"Path '{path}' does not exists"));

            string end = "*." + exp;

            string[] list = Directory.GetFiles(path, end);
		    if (list.Length > 0)
			    return list[0]; // Возвращаем первый попавшийся
		    else
                throw new Exception($"File does not exist: '*.{exp}'");
        }
    }
}
