﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentTranslation.StarDict
{
    /// <summary>
    /// Маркер может быть составным (к примеру, sametypesequence = tm).
    /// Виды одно-символьныx идентификаторов  словарных статей (для всех строчных идентификаторов текст в формате utf-8, заканчивается '\0'):
    /// 'm' - просто текст в кодировке utf-8, заканчивается '\0' 
    /// 'l' - просто текст в НЕ в кодировке utf-8, заканчивается '\0' 
    /// 'g' - текст размечен с помощью языка разметки текста Pango
    /// 't' - транскрипция в кодировке utf-8, заканчивается '\0' 
    /// 'x' - текст в кодировке utf-8, размечен с помощью xdxf
    /// 'y' - текст в кодировке utf-8, содержит китайские(YinBiao) или японские (KANA) символы 
    /// 'k' - текст в кодировке utf-8, размечен с помощью  KingSoft PowerWord XML 
    /// 'w' - текст размечен с помощью  MediaWiki
    /// 'h' - текст размечен с помощью  Html
    /// 'n' - текст размечен формате для WordNet
    /// 'r' - текст содержит список ресурсов. Ресурсами могут быть файлы картинки (jpg), звуковые (wav), видео (avi), вложенные(bin) файлы и др.
    /// 'W' - wav файл
    /// 'P' - картинка
    /// 'X' - этот тип зарезервирован для экспериментальных расширений
    /// </summary>
    public class Dict : BaseStarDictItem
    {
        private string sameTypeSequence;
        public Dict( string pathToDict, string sameTypeSequence) 
            : base(pathToDict, "dict") 
        {
            // Маркер, определяющий форматирование словарной статьи
            this.sameTypeSequence = sameTypeSequence;
        }

        public string GetTranslation(long wordDataOffset, long wordDataSize) 
        { 
            try
            {
			    // Убеждаемся что смещение и размер данных неотрицательны и находятся в пределах размера файла .dict
			    CheckValidArguments(wordDataOffset, wordDataSize);

			    // Открываем файл .dict как бинарный
                using (StreamReader sr = new StreamReader(dictionaryFile))
                {
                    byte[] byteArray = new byte[wordDataSize];
                    sr.BaseStream.Seek(wordDataOffset, SeekOrigin.Begin);  // Смешаемся внутри файла до начала текста, относящегося к переводу слова
                    sr.BaseStream.Read(byteArray, 0, (int)wordDataSize);        // Читаем часть файла, относящегося к переводу слова
                    // TODO: получить слово в зависимости от указанной кодировки
                    return UTF8Encoding.UTF8.GetString(byteArray); // Вернем раскодированный в юникодную строку набор байтoв (encoding определен в базовом классе BaseDictionaryItem)
                }
	        }
		    catch(Exception)
            {
			    return null;
            }
        }
        private void CheckValidArguments(long wordDataOffset, long wordDataSize) 
        { 
		    if (wordDataOffset < 0)
			    throw new ArgumentException();

		    long endDataSize = wordDataOffset + wordDataSize;

		    if (wordDataOffset < 0 || wordDataSize < 0 || endDataSize > realFileSize)
               throw new Exception();
        }
    }
}
