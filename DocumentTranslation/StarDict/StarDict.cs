﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentTranslation.StarDict
{
    public class StarDictImpl
    {
        private Ifo Ifo;
        private Idx Idx;
        private Dict Dict;

        public StarDictImpl(string pathToDict) 
        {
            try
            {
                // Формат словаря DICT предусматривает 3 обязательных файла (.ifo, .idx, .dict) и 1 необязательный (.syn)
                // Если хотя бы один из обязательных словарей отсутствует, вызовется исключение и словарь не будет загружен

                // Создаем объект Ifo (он содержит настройки и мета-информацию о словаре) [Обязательный файл]
                Ifo = new Ifo(pathToDict);

                // Создаем объект Idx (он содержит отсортированный список всех слов и оффсеты для каждого слова в файле .dict) [Обязательный файл]

                Idx = new Idx(pathToDict, int.Parse(Ifo.wordCount), int.Parse(Ifo.idxFileSize), int.Parse(Ifo.idxOffsetBits));

                // Создаем объект Dict (он содержит текстовую информацией (сами слова, транскрипцию, значение), дополненную различными медиа-файлами и разметками других словарных форматов) [Обязательный файл]
                Dict = new Dict(pathToDict, Ifo.sameTypeSequence);

                // Создаем объект Syn (он содержит информацию о синонимах) [Необязательный файл]
                // Syn = Syn(pathToDict)
            }
            catch(Exception e)
            {
                Console.WriteLine($"Dictionary {pathToDict} was not loaded: {e}");
            }

            
        }

        public string Translate(string word)
        {
            // Приводим слово к нижнему регистру и убираем пробелы с начала и конца
            word = word.ToLower().Trim();

            // Получаем у объекта Idx координаты расположения слова внутри файла .dict
            long[] location = Idx.GetLocationWord(word);


            if (location[0] == 0 || location[1] == 0)
			    return null;

            // Получаем у объекта Dict сам перевод и возвращаем его		 
            return Dict.GetTranslation(location[0], location[1]);
        }

        
    }
}
