﻿using DocumentTranslation.Frequency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentTranslation.StarDict
{
    public class Ifo: BaseStarDictItem
    {
        private IniParser iniParser;
        public string bookName;
        public string wordCount;
        public string synWordCount;
        public string idxFileSize;
        public string idxOffsetBits;
        public string author;
        public string email;
        public string description;
        public string date;
        public string sameTypeSequence;
        public string dictType;

        public Ifo(string pathToDict)
            : base(pathToDict, "ifo")
        {


            // Создаем и инициализируем парсер
            iniParser = new IniParser(dictionaryFile);

            // Считаем из ifo файла параметры
            // Если хотя бы одно из обязательных полей отсутствует, вызовется исключение и словарь не будет загружен
            bookName = GetParameterValue("bookname", null);                     // Название словаря [Обязательное поле]
            wordCount = GetParameterValue("wordcount", null); 				// Количество слов в ".idx" файле [Обязательное поле]
		    synWordCount = GetParameterValue("synwordcount", ""); 			// Количество слов в ".syn" файле синонимов [Обязательное поле, если есть файл ".syn"]
		    idxFileSize = GetParameterValue("idxfilesize", null); 			// Размер (в байтах) ".idx" файла. Если файл сжат архиватором, то здесь указывается размер исходного несжатого файла [Обязательное поле]
		    idxOffsetBits = GetParameterValue("idxoffsetbits", "32"); 			// Размер числа в битах(32 или 64), содержащего внутри себя смещение до записи в файле .dict. Поле пояилось начиная с версии 3.0.0, до этого оно всегда было 32 [Необязательное поле]
		    author = GetParameterValue("author", "");						// Автор словаря [Необязательное поле]
		    email = GetParameterValue("email", "");							// Почта [Необязательное поле]
		    description = GetParameterValue("description", "");				// Описание словаря [Необязательное поле]
		    date = GetParameterValue("date", "");							// Дата создания словаря [Необязательное поле]
		    sameTypeSequence = GetParameterValue("sametypesequence", null);	// Маркер, определяющий форматирование словарной статьи[Обязательное поле]
		    dictType = GetParameterValue("dicttype", "");					// Параметр используется некоторыми словарными плагинами, например WordNet[Необязательное поле]			
        }

        private string GetParameterValue(string key, string defaultValue)
        {
            try
            {
                return iniParser.GetValue(key);
            }
			catch
            {
                if (defaultValue != null)
                    return defaultValue;
                throw new Exception($"\n{dictionaryFile} has invalid format (missing parameter: {key}");
            }
            
        }
		
    }
}
