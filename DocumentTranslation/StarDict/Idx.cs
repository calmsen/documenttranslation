﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentTranslation.StarDict
{
    /// <summary>
    /// # Каждая запись внутри .idx файла состоит из 3-х полей, идущих друг за другом
    /// word_str;  		// Строка в формате utf-8, заканчивающаяся '\0'
    /// word_data_offset; // Смещение до записи в файле .dict (размер числа 32 или 64 бита)
    /// word_data_size;  	// Размер всей записи в файле .dict
    /// </summary>
    public class Idx : BaseStarDictItem
    {
        private Dictionary<string, long[]> idxDict;
        private int idxFileSize;
        private int idxOffsetBytes;
        private int wordCount;
        public Idx(string pathToDict, int wordCount, int idxFileSize, int idxOffsetBits)
            : base(pathToDict, "idx")
        {
            idxDict = new Dictionary<string, long[]>();							// Словарь, self.idxDict = {'иностр.слово': [Смещение_до_записи_в_файле_dict, Размер_всей_записи_в_файле_dict], ...}	
            this.idxFileSize = idxFileSize; 		// Размер файла .idx, записанный в .ifo файле
            idxOffsetBytes = idxOffsetBits / 8; // Размер числа, содержащего внутри себя смещение до записи в файле .dict. Переводим в байты и приводим к числу
            this.wordCount = wordCount;             // Количество слов в ".idx" файле

            // Проверяем целостность словаря (информация в .ifo файле о размере .idx файла [idxfilesize] должна совпадать с его реальным размером)
            CheckRealFileSize();

            // Заполняем словарь self.idxDict данными из файла .idx
            FillIdxDict();

            // Проверяем целостность словаря (информация в .ifo файле о количестве слов [wordcount] должна совпадать с реальным количеством записей в .idx файле)
            CheckRealWordCount();
        }

        /// <summary>
        /// Функция сверяет размер файла, записанный в .ifo файле, с ее реальным размером и в случае расхождений генерирует исключение
        /// </summary>
        private void CheckRealFileSize()
        {
            if (realFileSize != idxFileSize)
                throw new Exception($"size of the {dictionaryFile} is incorrect");
        }

        /// <summary>
        /// Функция сверяет количестве слов, записанное в .ifo файле, с реальным количеством записей в файле .idx и в случае расхождений генерирует исключение
        /// </summary>
        private void CheckRealWordCount()
        {
            int realWordCount = idxDict.Count();

            if (realWordCount != wordCount)
                throw new Exception($"word count of the {dictionaryFile} is incorrect");
        }

        /// <summary>
        /// Функция считывает из потока данных массив байтов заданной длины, затем преобазует байткод в число
        /// </summary>
        private long GetIntFromByteArray(int sizeInt, FileStream stream )
        {
            // Получили массив байтов, отведенных под число
            byte[] byteArray = new byte[sizeInt];
            for (int i = 0; i < byteArray.Length; i++)
                byteArray[i] = (byte)stream.ReadByte();

            if (BitConverter.IsLittleEndian)
                Array.Reverse(byteArray);

            long number = sizeInt == 8
                ? BitConverter.ToInt64(byteArray, 0)
                : BitConverter.ToInt32(byteArray, 0);

            return number;
        }

        /// <summary>
        /// Функция разделяет файл .idx на отдельные записи (запись состоит из 3-х полей) и каждую запись добавляет в словарь idxDict
        /// </summary>
        private void FillIdxDict()
        {
            List<byte> languageWord = new List<byte>();
            using (FileStream stream = new FileStream(dictionaryFile, FileMode.Open, FileAccess.Read))
            {
                while (true)
                {
                    int b = stream.ReadByte(); 	// Читаем один байт
				    if (b == -1) break;  	// Если байтов больше нет, то выходим из цикла
				    if (b > 0)		// Если байт не является символом окончания строки '\0', то прибавляем его к слову
					    languageWord.Add((byte)b);
                    else
                    {
                        // Если дошли до '\0', то считаем, что слово закончилось и дальше идут два числа ("Смещение до записи в файле dict" и "Размер всей записи в файле dict")
                        long wordDataOffset = GetIntFromByteArray(idxOffsetBytes, stream);  // Получили первое число "Смещение до записи в файле dict"
                        long wordDataSize = GetIntFromByteArray(4, stream);                      // Получили второе число "Размер всей записи в файле dict"
                        string languageWordAsString =  UTF8Encoding.UTF8.GetString(languageWord.ToArray());

                        idxDict[languageWordAsString] = new long[] { wordDataOffset, wordDataSize }; // Добавим в словарь self.idxDict запись: иностранное слово + смещение + размер данных
                        languageWord.Clear(); 											// Обнуляем переменную, поскольку начинается следующая струтура
                    } 
					    
                }
            }
        }

        /// <summary>
        /// Функция возвращает расположение слова в файле .dict ("Смещение до записи в файле dict" и "Размер всей записи в файле dict").
        /// Если такого слова в словаре нет, функция возвращает None
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public long[] GetLocationWord(string word)
        {
            try
            {
                return idxDict[word];
            }
            catch
            {
                return new long[] { 0, 0 };
            }

        }
    }
}
