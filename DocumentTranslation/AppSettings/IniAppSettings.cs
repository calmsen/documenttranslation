﻿using DocumentTranslation.Frequency;

namespace DocumentTranslation
{
    public class IniAppSettings : IAppSettings
    {
        public string PathToBooks { get; private set; }
        public int CountWord { get; private set; }
        public string PathToWordNetDict { get; private set; }
        public string PathToStarDict { get; private set; }

        private readonly string _settings;

        public IniAppSettings(string settings)
        {
            _settings = settings;
        }

        public void Init()
        {
            // Создаем и инициализируем конфиг-парсер
            var config = new IniParser(_settings);

            PathToBooks = config.GetValue("PathToBooks");               // Считываем из ini файла переменную PathToBooks, которая содержит  путь до файлов(книг, документов и тд), из которых будут браться слова		
            CountWord = int.Parse(config.GetValue("CountWord"));        // Считываем из ini файла переменную CountWord, которая содержит количество первых слов частотного словаря, которые нужно получить
            PathToWordNetDict = config.GetValue("PathToWordNetDict");   // Считываем из ini файла переменную PathToWordNetDict, которая содержит путь до словаря WordNet
            PathToStarDict = config.GetValue("PathToStarDict");         // Считываем из ini файла переменную PathToStarDict, которая содержит путь до словарей в формате StarDict		
        }

    }
}
