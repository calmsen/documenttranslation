﻿using System;
using System.Collections.Specialized;
using System.IO;

namespace DocumentTranslation
{
    public class NameValueAppSettings : IAppSettings
    {
        public string PathToBooks { get; private set; }
        public int CountWord { get; private set; }
        public string PathToWordNetDict { get; private set; }
        public string PathToStarDict { get; private set; }
        
        private readonly NameValueCollection _nameValueCollection;

        public NameValueAppSettings(NameValueCollection nameValueCollection)
        {
            _nameValueCollection = nameValueCollection;
        }

        public void Init()
        {
            PathToBooks = GetValue("PathToBooks");               // Считываем из ini файла переменную PathToBooks, которая содержит  путь до файлов(книг, документов и тд), из которых будут браться слова		
            CountWord = int.Parse(GetValue("CountWord"));        // Считываем из ini файла переменную CountWord, которая содержит количество первых слов частотного словаря, которые нужно получить
            PathToWordNetDict = GetValue("PathToWordNetDict");   // Считываем из ini файла переменную PathToWordNetDict, которая содержит путь до словаря WordNet
            PathToStarDict = GetValue("PathToStarDict");         // Считываем из ini файла переменную PathToStarDict, которая содержит путь до словарей в формате StarDict		
        }

        private string GetValue(string name)
        {
            return _nameValueCollection[name].Replace("|DataDirectory|", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data"));
        }
    }
}
