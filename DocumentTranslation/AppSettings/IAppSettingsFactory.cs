﻿namespace DocumentTranslation
{
    public interface IAppSettingsFactory
    {
        IAppSettings Create();
    }
}