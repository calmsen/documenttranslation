﻿
using System.Collections.Specialized;

namespace DocumentTranslation
{
    public class NameValueAppSettingsFactory : IAppSettingsFactory
    {
        private readonly NameValueCollection _nameValueCollection;

        public NameValueAppSettingsFactory(NameValueCollection nameValueCollection)
        {
            _nameValueCollection = nameValueCollection;
        }
        
        public IAppSettings Create()
        {
            NameValueAppSettings appSettings = new NameValueAppSettings(_nameValueCollection);
            appSettings.Init();
            return appSettings;
        }

    }
}
