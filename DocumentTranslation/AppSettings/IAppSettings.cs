﻿using Database;
using Database.Migrations;
using Database.Models;

namespace DocumentTranslation
{
    public interface IAppSettings
    {
        string PathToBooks { get; }
        int CountWord { get; }
        string PathToWordNetDict { get; }
        string PathToStarDict { get; }
    }
}
