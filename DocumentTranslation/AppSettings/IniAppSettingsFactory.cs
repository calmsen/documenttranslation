﻿
namespace DocumentTranslation
{
    public class IniAppSettingsFactory : IAppSettingsFactory
    {
        private readonly string _settings;

        public IniAppSettingsFactory(string settings)
        {
            _settings = settings;
        }
        
        public IAppSettings Create()
        {
            IniAppSettings appSettings = new IniAppSettings(_settings);
            appSettings.Init();
            return appSettings;
        }

    }
}
