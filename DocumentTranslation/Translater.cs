﻿using Database;
using Database.Models;
using DocumentTranslation.Frequency;
using DocumentTranslation.StarDict;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DocumentTranslation
{
    public class Translater
    {
        private IAppSettingsFactory appSettingsFactory;
        private IAppSettings appSettings;
        private List<StarDictImpl> listLanguageDict;

        public Translater(IAppSettingsFactory appSettingsFactory)
        {
            this.appSettingsFactory = appSettingsFactory;
        }
        
        /// <summary>
        /// Метод создает список файлов, расположенных в папке path	
        /// </summary>
        private string[] GetAllFiles(string path)
        {
            try
            {
                return Directory.GetFiles(path).ToArray();
            }
			catch
            {
                throw new Exception($"Path '{ path }' does not exists");
            }
        }

        /// <summary>
        /// Метод бежит по всем словарям, и возвращает перевод из ближайшего словаря. Если перевода нет ни в одном из словарей, возвращается пустая строка	
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        private string GetTranslate(string word)
        {
            string valueWord = null;

            foreach (var dict in listLanguageDict)
            {
                valueWord = dict.Translate(word);

                if (!string.IsNullOrEmpty(valueWord))
                    return valueWord.Substring(valueWord.IndexOf("</k>") + "</k>".Length);
            }

            return valueWord;
        }

        /// <summary>
        /// Метод возвращает транскрипцию, при этом транскрипция удаляется из источника
        /// </summary>
        /// <param name="translation"></param>
        /// <returns></returns>
        private string GetTranscription(ref string translation)
        {
            if (string.IsNullOrEmpty(translation))
                return null;

            const string trStartTag = "<tr>";
            const string trEndTag = "</tr>";
            
            if (translation.IndexOf(trEndTag) == -1)
                return null;

            string[] translationParts = translation.Split(new string[] { trEndTag }, StringSplitOptions.None);
            if (translationParts.Length != 2)
                return null;

            string transcription = translationParts[0].Replace(trStartTag, "").Trim();
            if (string.IsNullOrEmpty(transcription))
                return null;

            if (transcription.Length > 150)
                return null;

            translation = translationParts[1].Trim();

            return transcription;
            
        }

        /// <summary>
        /// Метод сохраняет результат(само слово, частота, его перевод) по первым countWord словам в файл  
        /// </summary>
        private void SaveResultToDb(Dictionary<string, int> mostCommonElements)
        {
            if (mostCommonElements == null)
                return;

            try
            {
                using (var db = new DefaultDatabase())
                {
                    foreach (var item in mostCommonElements)
                    {
                        string translation = GetTranslate(item.Key);
                        if (string.IsNullOrEmpty(translation))
                            continue;

                        EnglishWord word = db.EnglishWords.FirstOrDefault(x => x.Word == item.Key);
                        if (word != null)
                            continue;

                        string transcription = GetTranscription(ref translation);

                        db.EnglishWords.Add(new EnglishWord
                        {
                            Word = item.Key,
                            Transcription = transcription,
                            Translation = translation
                        });
                    }
                    db.SaveChanges();
                }
            }
			catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Метод запускает задачу на выполнение
        /// </summary>
        public void Run()
        {
            // Получаем список книг, из которых будем получать слова
            string[] listBooks = GetAllFiles(appSettings.PathToBooks);
            Run(listBooks);
        }

        /// <summary>
        /// Метод запускает задачу на выполнение
        /// </summary>
        public void Run(string pathToBook)
        {
            if (string.IsNullOrEmpty(pathToBook))
                return;
            Run(new string[] { pathToBook });
        }

        /// <summary>
        /// Метод запускает задачу на выполнение
        /// </summary>
        public void Run(Stream book)
        {
            string pathToBook = null;
            Run(new string[] { pathToBook });
        }

        /// <summary>
        /// Метод запускает задачу на выполнение
        /// </summary>
        public void Run(string[] listBooks)
        {
            // инициализируем настройки
            appSettings = appSettingsFactory.Create();

            listLanguageDict = new List<StarDictImpl>();  // В этом массиве сохраним словари StarDict

            try
            {
                // Отделяем пути словарей StarDict друг от друга и удаляем пробелы с начала и конца пути. Все пути заносим в список listPathToStarDict
                string[] listPathToStarDict = appSettings.PathToStarDict.Split(';').Select(x => x.Trim()).ToArray();

                // Для каждого из путей до словарей StarDict создаем свой языковый словарь
                foreach (string path in listPathToStarDict)
                {
                    var languageDict = new StarDictImpl(path);

                    listLanguageDict.Add(languageDict);
                }



                // Создаем частотный словарь		
                var frequencyDict = new FrequencyDict(appSettings.PathToWordNetDict);

                // Подготовка закончена, загружены словари StarDict и WordNet. Запускаем задачу на выполнение, то есть начинаем парсить текстовые файл, нормализовывать и считать слова			

                // Отдаем частотному словарю по одной книге	
                foreach (var book in listBooks)
                    frequencyDict.ParseBook(book);

                // Получаем первые countWord слов из всего получившегося списка английских слов			
                Dictionary<string, int> mostCommonElements = frequencyDict.FindMostCommonElements(appSettings.CountWord);

                // Запишем результат в базу данных
                SaveResultToDb(mostCommonElements);

            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e}");
            }
        }
		    
    }
}
