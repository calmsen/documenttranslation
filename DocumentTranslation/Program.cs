﻿using Database.Migrations;
using System;

namespace DocumentTranslation
{
    class Program
    {
        static void Main(string[] args)
        {
            var migration = new MyContextMigration();
            migration.Migrate();

            var appSettingsFactory = new IniAppSettingsFactory("Settings.ini");
            var translater = new Translater(appSettingsFactory);
            translater.Run();

            Console.WriteLine("Program is accomplished");
            Console.ReadKey();
        }    
    }
}
