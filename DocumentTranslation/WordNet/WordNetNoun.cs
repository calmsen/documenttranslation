﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentTranslation.Utils;

namespace DocumentTranslation.WordNet
{
    /// <summary>
    /// Класс для работы с нормализацией существительных
    /// Класс наследуется от BaseWordNetItem
    /// </summary>
    public class WordNetNoun : BaseWordNetItem
    {
        public WordNetNoun(string pathToWordNetDict)
            : base(pathToWordNetDict, "data.noun", "index.noun")
        {
            // Правила замены окончаний при нормализации слова по правилам. К примеру, окончание "s" заменяется на "", "ses" заменяется на "s" и тд.
            rule = new string[][]
            {	
				new string[]{"s"    , ""    },
				new string[]{"’s"   , ""    },
				new string[]{"’"    , ""    },
				new string[]{"ses"  , "s"   },
				new string[]{"xes"  , "x"   },
				new string[]{"zes"  , "z"   },
				new string[]{"ches" , "ch"  },
				new string[]{"shes" , "sh"  },
				new string[]{"men"  , "man" },
				new string[]{"ies"  , "y"   }
            };
        }

        /// <summary>
        /// Метод возвращает лемму сушествительного(нормализованную форму слова)
        /// Этот метод есть в базовом классе BaseWordNetItem, но нормализация существительных несколько отличается от нормализации других частей речи, 
        /// поэтому метод в наследнике переопределен
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public override string GetLemma(string word)
        {

            word = word.Trim().ToLower();

            // Если существительное слишком короткое, то к нормализованному виду мы его не приводим	
            if (word.Length <= 2)
                return null;

            // Если существительное заканчивается на "ss", то к нормализованному виду мы его не приводим	
            if (word.EndsWith("ss"))
                return null;

            // Пройдемся по кэшу, возможно слово уже нормализовывалось раньше и результат сохранился в кэше
            string lemma = GetDictValue(cacheWords, word);
            if (lemma != null)
                return lemma;

            // Проверим, если слово уже в нормализованном виде, вернем его же
            if (IsDefined(word))
                return word;

            // Пройдемся по исключениям, если слово из исключений, вернем его нормализованную форму
            lemma = GetDictValue(wordNetExcDict, word);
            if (lemma != null)
                return lemma;


            // Если существительное заканчивается на "ful", значит отбрасываем "ful", нормализуем оставшееся слово, а потом суффикс приклеиваем назад.
            // Таким образом, к примеру, из слова "spoonsful" после нормализации получится "spoonful"
            string suff = "";
            if (word.EndsWith("ful"))
            {
                word = word.TrimEnd("ful");	// Отрезаем суффикс "ful"
                suff = "ful";		// Отрезаем суффикс "ful", чтобы потом приклеить назад
            }


            // На этом шаге понимаем, что слово не является исключением и оно не нормализовано, значит начинаем нормализовывать его по правилам. 
            lemma = RuleNormalization(word);
            if (lemma != null)
            {
                lemma += suff;					// Не забываем добавить суффикс "ful", если он был
                cacheWords[word] = lemma; 	// Предварительно добавим нормализованное слово в кэш
                return lemma;
            }

            return null;
        }
    }
}
