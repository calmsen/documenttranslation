﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentTranslation.Utils;

namespace DocumentTranslation.WordNet
{
    public class BaseWordNetItem
    {
        protected string[][] rule;
        protected Dictionary<string, string> wordNetExcDict;
        protected List<string> wordNetIndexDict;
        protected string excFile;
        protected string indexFile;
        protected Dictionary<string, string> cacheWords;
        public BaseWordNetItem(string pathWordNetDict, string excFile, string indexFile)
        {
            rule = new string[0][];												// Правила замены окончаний при нормализации слова по правилам.

            wordNetExcDict = new Dictionary<string, string>(); 									// Словарь исключений
            wordNetIndexDict = new List<string>(); 									// Индексный массив	

            excFile = Path.Combine(pathWordNetDict, excFile);		// Получим путь до файла исключений	
            indexFile = Path.Combine(pathWordNetDict, indexFile);	// Получим путь до индексного словаря

            ParseFile(excFile, AppendExcDict);		// Заполним словарь исключений
            ParseFile(indexFile, AppendIndexDict);	// Заполним индексный массив 

            cacheWords = new Dictionary<string, string>();
        }
        /// <summary>
        /// Метод добавляет в словарь исключений одно значение. 
        /// Файл исключений представлен в формате: [слово-исключение][пробел][лемма]	
        /// </summary>
        /// <param name="line"></param>
        protected void AppendExcDict(string line)
        {
            // При разборе строки из файла, каждую строку разделяем на 2 слова и заносим слова в словарь(первое слово - ключ, второе - значение). При этом не забываем убрать с концов пробелы
            string[] group = line.Replace("\n", "").Split(' ');
            wordNetExcDict[group[0].Trim()] = group[1].Trim();
        }
        /// <summary>
        /// Метод добавляет в индексный массив одно значение.
        /// </summary>
        /// <param name="line"></param>
        public void AppendIndexDict(string line)
        {
            // На каждой строке берем только первое слово
            string[] group = line.Split(' ');
            wordNetIndexDict.Add(group[0].Trim());
        }
        /// <summary>
        /// Метод открывает файл на чтение, читает по одной строке и вызывает для каждой строки функцию, переданную в аргументе
        /// </summary>
        /// <param name="file"></param>
        /// <param name="contentHandler"></param>
        public void ParseFile(string file, Action<string> contentHandler)
        {
            try
            {
                using (StreamReader reader = new StreamReader(file))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                        contentHandler(line);   // Для каждой строки вызываем обработчик контента
                }
            }
            catch
            {
                throw new Exception($"File does not load: '{file}'");
            }

        }
        /// <summary>
        /// Метод возвращает значение ключа в словаре. Если такого ключа в словаре нет, возвращается пустое значение. 
        /// Под словарем здесь подразумевается просто структура данных 
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="key"></param>
        protected string GetDictValue(Dictionary<string, string> dict, string key)
        {
            if (dict.ContainsKey(key))
                return dict[key];
            return null;
        }
        /// <summary>
        /// Метод проверяет слово на существование, и возвращает либо True, либо False.
        /// Для того, чтобы понять, существует ли слово, проверяется индексный массив(там хранится весь список слов данной части речи).	
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        protected bool IsDefined(string word)
        {
            if (wordNetIndexDict.IndexOf(word) >= 0)
                return true;
            return false;
        }

        /// <summary>
        /// Метод возвращает лемму(нормализованную форму слова)			
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public virtual string GetLemma(string word)
        {
            word = word.Trim().ToLower();

            // Пустое слово возвращаем обратно
            if (word == null)
                return null;

            // Пройдемся по кэшу, возможно слово уже нормализовывалось раньше и результат сохранился в кэше
            string lemma = GetDictValue(cacheWords, word);
            if (lemma != null)
                return lemma;

            // Проверим, если слово уже в нормализованном виде, вернем его же
            if (IsDefined(word))
                return word;


            // Пройдемся по исключениям, если слово из исключений, вернем его нормализованную форму
            lemma = GetDictValue(wordNetExcDict, word);
            if (lemma != null)
                return lemma;


            // На этом шаге понимаем, что слово не является исключением и оно не нормализовано, значит начинаем нормализовывать его по правилам. 
            lemma = RuleNormalization(word);
            if (lemma != null)
            {
                cacheWords[word] = lemma; 	// Предварительно добавим нормализованное слово в кэш
                return lemma;
            }

            return null;
        }

        /// <summary>
        /// Нормализация слова по правилам (согласно грамматическим правилам, слово приводится к нормальной форме)
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        protected string RuleNormalization(string word)
        {
            // Бежим по всем правилам, смотрим совпадает ли окончание слова с каким либо правилом, если совпадает, то заменяем окончние.	
            foreach (string[] replGroup in rule)
            {
                string endWord = replGroup[0];
                if (word.EndsWith(endWord))
                {
                    string lemma = word.TrimEnd(endWord);	// Отрезаем старое окончание
                    lemma += replGroup[1];			// Приклеиваем новое окончание
                    if (IsDefined(lemma))		// Проверим, что получившееся новое слово имеет право на существование, и если это так, то вернем его
                        return lemma;
                }
            }
            return null;
        }
    }
}
