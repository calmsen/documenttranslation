﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentTranslation.WordNet
{
    /// <summary>
    /// Класс для работы с нормализацией прилагательных
    /// Класс наследуется от BaseWordNetItem
    /// </summary>
    public class WordNetAdjective :BaseWordNetItem
    {
        public WordNetAdjective(string pathToWordNetDict)
            : base(pathToWordNetDict, "data.adj", "index.adj") 
        { 
            // Правила замены окончаний при нормализации слова по правилам. К примеру, окончание "er" заменяется на "" или  "e" и тд.
            rule = new string[][]
            {	
				new string[]{"er"  , "" },
				new string[]{"er"  , "e"},
				new string[]{"est" , "" },
				new string[]{"est" , "e"}
            };
        }
    }
}
