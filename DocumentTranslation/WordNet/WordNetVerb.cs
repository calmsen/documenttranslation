﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentTranslation.WordNet
{
    /// <summary>
    /// Класс для нормализации глаголов
    /// Класс наследуется от BaseWordNetItem
    /// </summary>
    public class WordNetVerb : BaseWordNetItem
    {
        public WordNetVerb(string pathToWordNetDict)
            : base(pathToWordNetDict, "data.verb", "index.verb")
        {
            // Правила замены окончаний при нормализации слова по правилам. К примеру, окончание "s" заменяется на "" , "ies" на и "y" тд.
            rule = new string[][]
            {	
				new string[]{"s"   , ""  },
				new string[]{"ies" , "y" },
				new string[]{"es"  , "e" }, 
				new string[]{"es"  , ""  },	
				new string[]{"ed"  , "e" }, 
				new string[]{"ed"  , ""  },
				new string[]{"ing" , "e" },
				new string[]{"ing" , ""  }
            };
        }
    }
}
