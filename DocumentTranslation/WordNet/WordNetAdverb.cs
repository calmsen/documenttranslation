﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentTranslation.WordNet
{
    /// <summary>
    /// Класс для нормалзации наречий
    /// Класс наследуется от BaseWordNetItem
    /// </summary>
    public class WordNetAdverb : BaseWordNetItem
    {
        public WordNetAdverb(string pathToWordNetDict)
            : base(pathToWordNetDict, "data.adv", "index.adv") 
        { 
            // У наречий есть только списки исключений(adv.exc) и итоговый список слов(index.adv).	
		    // Правила замены окончаний при нормализации слова по правилам у наречий нет. 
        }
    }
}
