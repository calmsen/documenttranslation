﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentTranslation.WordNet
{
    public class Lemmatizer
    {
        private char splitter = '-'; // Разделитель составных слов	
        private BaseWordNetItem[] wordNet;
        public Lemmatizer(string pathToWordNetDict) 
        {
		    // Инициализируем объекты с частям речи
		    BaseWordNetItem adj = new WordNetAdjective(pathToWordNetDict);	// Прилагательные
		    BaseWordNetItem noun = new WordNetNoun(pathToWordNetDict);		// Существительные
		    BaseWordNetItem adverb = new WordNetAdverb(pathToWordNetDict);	// Наречия
		    BaseWordNetItem verb = new WordNetVerb(pathToWordNetDict);		// Глаголы

            wordNet = new BaseWordNetItem[] { verb, noun, adj, adverb };
        }
        /// <summary>
        /// Метод возвращает лемму слова (возможно, составного)		
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
	    public string GetLemma(string word)
        {
		    // Если в слове есть тире, разделим слово на части, нормализуем каждую часть(каждое слово) по отдельности, а потом соединим
		    string[] wordArr = word.Split(splitter);
		    List<string> resultWord = new List<string>();
		    foreach (string w in wordArr)
            {
			    string lemma = GetLemmaWord(w);
			    if (lemma != null)
				    resultWord.Add(lemma);
		    }
            if (resultWord.Count > 0)
			    return string.Join(splitter.ToString(), resultWord);
            return null;
		}
        /// <summary>
        /// Метод возвращает лемму(нормализованную форму слова)			
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
	    public string GetLemmaWord(string word)
        {
		    foreach (BaseWordNetItem item in wordNet)
            {
			    string lemma = item.GetLemma(word);
			    if (lemma != null)
				    return lemma;
            }
            return null;
        }
    }
}
