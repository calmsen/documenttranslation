﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DocumentTranslation.Frequency
{
    public class IniParser
    {
        private string file;
        private Dictionary<string, string> IniDict; 
        public IniParser(string file)
        {
            this.file = file;
            IniDict = new Dictionary<string, string>();
            FillIniDict(file);
        }

        public string GetValue(string key)
        {
            try
            {
                return IniDict[key];
            }
            catch (Exception e)
            {
                throw new Exception($"{key} does not exists", e);
            }
        }
        private void FillIniDict(string file)
        {
            var keyValRegEx = new Regex(@"^\s*(.+?)\s*=\s*(.+?)\s*$");

            foreach (var line in ReadFileAsListLine(file))
            {
                var result = keyValRegEx.Match(line);

                if (result.Success)
                    IniDict[result.Groups[1].Value] = result.Groups[2].Value;
            }                
        }
        private string[] ReadFileAsListLine(string file)
        {
            try
            {
                return File.ReadAllLines(file);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

    }
}
