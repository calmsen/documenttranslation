﻿using DocumentTranslation.WordNet;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DocumentTranslation.Frequency
{
    /// <summary>
    /// Простое слово, например "over", можно найти, используя выражение "([a-zA-Z]+)" - здесь ищется одна или более букв английского алфавита.
    /// Составное слово, к примеру "commander-in-chief", найти несколько сложнее, нам нужно искать идущие друг за другом 
    /// подвыражения вида "commander-", "in-", после которых идет слово "chief".
    /// Регулярное выражение примет вид "(([a-zA-Z]+-?)*[a-zA-Z]+)".
    /// Если в выражении присутсвует промежуточное подвыражение, оно тоже включается в результат. Так, в наш результат попадает не только слово 
    /// "commander-in-chief", но также и все найденные подвыражения, Чтобы их исключить, добавим в начале подвыражеения '?:' стразу после открывающейся круглой скобки.		
    /// Тогда регулярное выражение примет вид "((?:[a-zA-Z]+-?)*[a-zA-Z]+)".
    /// Нам еще осталось включить в выражения слова с апострофом вида "didn't".		
    /// Для этого заменим в первом подвыражении "-?" на "[-']?".
    /// Все, на этом закончим улучшения регулярного выражения, его можно было бы улучшать и дальше, но остановимся на таком: 
    /// "((?:[a-zA-Z]+[-']?)*[a-zA-Z]+)"
    /// </summary>
    public class FrequencyDict
    {
        private Regex wordPattern;
        private Dictionary<string, int> frequencyDict;
        private Lemmatizer lemmatizer;
        public FrequencyDict(string pathToWordNetDict) 
        { 
            // Определяем регулярное выражение для поиска английских слов
		    wordPattern = new Regex("((?:[a-zA-Z]+[-']?)*[a-zA-Z]+)", RegexOptions.Compiled);
		
		    // Частотный словарь(использум класс collections.Counter для поддержки подсчёта уникальных элементов в последовательностях) 		
            frequencyDict = new Dictionary<string, int>();

		    // Создаем нормализатор английских слов
            lemmatizer = new Lemmatizer(pathToWordNetDict);
        }
        /// <summary>
        /// Метод парсит файл, получает из нее слова
        /// </summary>
        /// <param name="file"></param>
        public void ParseBook(string file) 
        {
            if (file.EndsWith(".txt"))
                ParseTxtFile(file, FindWordsFromContent);
            else if (file.EndsWith(".pdf"))
                ParsePdfFile(file, FindWordsFromContent);
            else
                throw new ArgumentException(string.Format("The file format is not supported: {0}", file));
        }
        /// <summary>
        /// Метод парсит файл в формате txt
        /// </summary>
        private void ParseTxtFile(string txtFile, Action<string> contentHandler) 
        {
            try
            {
                using (StreamReader reader = new StreamReader(txtFile))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                        contentHandler(line);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error parsing {txtFile}: {e}");
            }
            
        }
        /// <summary>
        /// Метод парсит файл в формате pdf
        /// </summary>
        /// <param name="pdfFile"></param>
        /// <param name="contentHandler"></param>
        private void ParsePdfFile(string pdfFile, Action<string> contentHandler) 
        {
            using (PdfReader reader = new PdfReader(pdfFile))
            {
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    string text = PdfTextExtractor.GetTextFromPage(reader, i);
                    contentHandler(text);
                }
            }
        }
        /// <summary>
        /// Метод находит в строке слова согласно своим правилам, нормализует их и затем добавляет в частотный словарь
        /// </summary>
        private void FindWordsFromContent(string content) 
        {
            var result = wordPattern.Matches(content); 	// В строке найдем список английских слов				
            foreach (Match w in result) 
            {
			    string word = w.Value.ToLower();						// Приводим слово к нижнему регистру	
			    string lemma = lemmatizer.GetLemma(word);   // Нормализуем слово

                if (!string.IsNullOrEmpty(lemma))
                    word = lemma;

                if (!frequencyDict.ContainsKey(word))
                    frequencyDict.Add(word, 0);

                frequencyDict[word] += 1;		// Добавляем в счетчик частотного словаря	
            }
	
        }
        /// <summary>
        /// Метод отдает первые countWord слов частотного словаря, отсортированные по ключу и значению
        /// </summary>
        /// <param name="countWord"></param>
        /// <returns></returns>
        public Dictionary<string, int> FindMostCommonElements(int countWord) 
        {
            return frequencyDict
                .OrderBy(x => x.Key)
                .ThenBy(x => x.Value)
                .Skip(0)
                .Take(countWord)
                .ToDictionary(x => x.Key, y => y.Value);
        }
    }
}
