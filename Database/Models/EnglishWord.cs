﻿using Database.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Models
{
    /// <summary>
    /// Описывает информацию о версиях базы данных 
    /// </summary>
    public class EnglishWord
    {
        /// <summary>
        /// Идентификатор 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Английское слово
        /// </summary>
        public string Word { get; set; }

        /// <summary>
        /// Транскрипция
        /// </summary>
        public string Transcription { get; set; }

        /// <summary>
        /// Перевод слова
        /// </summary>
        public string Translation { get; set; }

        /// <summary>
        /// Состояние
        /// </summary>
        public EnglishWordState State { get; set; }

        /// <summary>
        /// Владелец
        /// </summary>
        public string Owner { get; set; }
    }
}
