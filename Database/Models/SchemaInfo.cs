﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Models
{
    /// <summary>
    /// Описывает информацию о версиях базы данных 
    /// </summary>
    public class SchemaInfo
    {
        /// <summary>
        /// Идентификатор 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Версия базы данных
        /// </summary>
        public int Version { get; set; }
    }
}
