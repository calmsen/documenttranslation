﻿using Database.Models;
using System.Data.Entity;
using System.Diagnostics;

namespace Database
{
    /// <summary>
    /// Описывает контекст базы данных 
    /// </summary>
    public class DefaultDatabase : DbContext
    {
        /// <summary>
        /// Инициализирует экземпляр класс DefaultDatabase
        /// </summary>
        public DefaultDatabase() : base("DefaultConnection")
        {
            Database.Log = Logger;
        }

        /// <summary>
        /// Информацию о версиях базы данных 
        /// </summary>
        public DbSet<SchemaInfo> SchemaInfoes { get; set; }

        /// <summary>
        /// Английские слова
        /// </summary>
        public DbSet<EnglishWord> EnglishWords { get; set; }

        /// <summary>
        /// Настройка логирования для дебага
        /// </summary>
        /// <param name="logString"></param>
        private void Logger(string logString)
        {
            Debug.WriteLine(logString);
        }
    }
}
