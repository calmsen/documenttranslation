﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations
{
    /// <summary>
    /// Интерфейс описывает миграцию для локальной базы данных
    /// </summary>
    public interface IMigration
    {
        /// <summary>
        /// Версия базы данных
        /// </summary>
        int Version { get; }

        /// <summary>
        /// Sql команды (н-р create table, insert, update, delete )
        /// </summary>
        List<string> Commands { get; }
    }
}
