﻿using Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations
{
    /// <summary>
    /// Обновляет базу данных до требуемой версии
    /// </summary>
    public class MyContextMigration
    {
        /// <summary>
        /// Версия до которой нужно обновиться
        /// </summary>
        private int _requiredVersion = 1;

        /// <summary>
        /// Список всех миграций.
        /// </summary>
        private List<IMigration> _migrations;

        /// <summary>
        /// Создает экземпляр класса MyContextMigration
        /// </summary>
        public MyContextMigration()
        {
            RegisterMigrations();
        }

        /// <summary>
        /// Обновляет базу данных до требуемой версии   
        /// </summary>
        public void Migrate()
        {
            using (var db = new DefaultDatabase())
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Database.ExecuteSqlCommand(@"CREATE TABLE IF NOT EXISTS `SchemaInfoes` (
	                    `Id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	                    `Version`	INTEGER
                    );");
                    int currentVersion = 0;
                    if (db.SchemaInfoes.Count() > 0)
                        currentVersion = db.SchemaInfoes.Max(x => x.Version);

                    while (currentVersion < _requiredVersion)
                    {
                        currentVersion++;
                        var migration = _migrations.FirstOrDefault(x => x.Version == currentVersion);
                        if (migration == null)
                            continue;

                        foreach (string command in migration.Commands)
                        {
                            db.Database.ExecuteSqlCommand(command);
                        }
                        db.SchemaInfoes.Add(new SchemaInfo() { Version = currentVersion });
                    }
                    db.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                }

            }
        }

        /// <summary>
        /// Регистрирует миграции
        /// </summary>
        private void RegisterMigrations()
        {
            var type = typeof(IMigration);
            _migrations = type.Assembly
                .GetTypes()
                .Where(x => type.IsAssignableFrom(x) && x.IsClass)
                .Select(x => (IMigration)Activator.CreateInstance(x))
                .ToList();
        }
    }
}
