﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Migrations
{
    /// <summary>
    /// Первая миграция - создает таблицу LocalStorageDatas
    /// </summary>
    public class MigrationV1 : IMigration
    {
        /// <summary>
        /// Версия базы данных
        /// </summary>
        public int Version => 1;

        /// <summary>
        /// Sql команды (н-р create table, insert, update, delete )
        /// </summary>
        public List<string> Commands { get; } = new List<string>();

        public MigrationV1()
        {
            Commands.Add(@"CREATE TABLE `EnglishWords` (
                `Id`	            INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	            `Word`	            TEXT NOT NULL,
	            `Transcription`	    TEXT,
	            `Translation`	    TEXT NOT NULL,
	            `State`	            INTEGER,
                `Owner`	            TEXT
            );");
        }
    }
}
