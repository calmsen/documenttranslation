﻿using DocumentTranslation;
using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace WebApp.Controllers
{

    public class BookParserController : ApiController
    {
        public async Task<HttpResponseMessage> Post()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/App_Data/temp");
            Directory.CreateDirectory(root);

            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

                // This illustrates how to get the file names.
                foreach (MultipartFileData file in provider.FileData)
                {
                    Trace.WriteLine(file.Headers.ContentDisposition.FileName);
                    Trace.WriteLine("Server file path: " + file.LocalFileName);
                    int fileExtPos = file.Headers.ContentDisposition.FileName.IndexOf(".");
                    if (fileExtPos == -1)
                        throw new Exception();
                    string fileExt = file.Headers.ContentDisposition.FileName.Substring(fileExtPos).TrimEnd('"').TrimEnd('\\');
                    string localFileName = file.LocalFileName + fileExt;
                    File.Move(file.LocalFileName, localFileName);

                    var nameValueCollection = WebConfigurationManager.AppSettings as NameValueCollection;
                    var appSettingsFactory = new NameValueAppSettingsFactory(nameValueCollection);
                    var translater = new Translater(appSettingsFactory);
                    translater.Run(localFileName);
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }



    }
}
