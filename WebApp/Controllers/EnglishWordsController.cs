﻿using Database;
using Database.Models;
using Database.Models.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebApp.Models;
using WebApp.Models.Enums;

namespace WebApp.Controllers
{

    public class EnglishWordsController : ApiController
    {
        const int WORDS_IN_PAGE = 200;

        // GET api/englishwords
        public IEnumerable<EnglishWord> Get(int page = 1, EnglishWordFilter filter = EnglishWordFilter.None)
        {
            if (page < 1)
                page = 1;

            using (var db = new DefaultDatabase())
            {
                var query = db.EnglishWords.AsQueryable();

                if (filter != EnglishWordFilter.All) {
                    var state = MapFilterToState(filter);
                    query = query.Where(x => x.State == state);
                }

                return query
                    .OrderBy(x => x.Id)
                    .Skip(WORDS_IN_PAGE * (page - 1))
                    .Take(WORDS_IN_PAGE)
                    .ToList();
            }                
        }
        
        // POST api/englishwords
        public void Post([FromBody]EnglishWordPostAction data)
        {
            if (data == null || data.Action == EnglishWordActionEnum.None || data.WordIds == null || data.WordIds.Length == 0)
                return;

            using (var db = new DefaultDatabase())
            {
                List<EnglishWord> words = db.EnglishWords.Where(x => data.WordIds.Contains(x.Id)).ToList();
                var state = data.Action == EnglishWordActionEnum.Known
                    ? EnglishWordState.Known
                    : EnglishWordState.Unknown;
                words.ForEach(x => x.State = state);
                db.SaveChanges();
            }
        }

        private EnglishWordState MapFilterToState(EnglishWordFilter filter)
        {
            switch (filter)
            {
                case EnglishWordFilter.None:
                    return EnglishWordState.None;
                case EnglishWordFilter.Known:
                    return EnglishWordState.Known;
                case EnglishWordFilter.Unknown:
                    return EnglishWordState.Unknown;
                default:
                    return EnglishWordState.None;
            }
        }
    }
}
