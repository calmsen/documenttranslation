﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.Models.Enums;

namespace WebApp.Models
{
    public  class EnglishWordPostAction
    {
        public EnglishWordActionEnum Action { get; set; }
        public int[] WordIds { get; set; }
    }
}
