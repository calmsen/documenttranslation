﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models.Enums
{
    public enum EnglishWordFilter
    {
        None,
        Known,
        Unknown,
        All
    }
}