﻿$(function () {
    var actionInProgress = false;
    var currentPage = 0;
    var noMore = false;

    var filterControl = $("#english-words-filter-name");
    var actionControl = $("#english-words-action-name");
    var loading = $("#loading");
    // *********************************************************
    var player = $("<audio>")
        .attr("controls", "controls")
        .css("position", "absolute")
        .css("top", "-10000px")
        .appendTo("body")[0];

    function getWords(refresh) {
        refresh = refresh || false;
        if (refresh) {
            currentPage = 0;
            noMore = false;
            loading.show();
        }

        currentPage++;

        $.ajax({
            url: "/api/englishwords",
            type: "get",
            dataType: "json",
            data: {
                filter: parseInt(filterControl.val()),
                page: currentPage
            },
            success: function (words) {
                if (!words)
                    return;

                if (words.length === 0) {
                    noMore = true;
                    loading.hide();
                }                    

                var tbody = $("#english-words-tbody");
                if (refresh) tbody.empty();

                var counter = tbody.children().length;
                for (var i = 0; i < words.length; i++) {
                    tbody.append(
                        $("<tr>")
                            .append($("<td>").append($("<input>").attr("type", "checkbox").attr("id", words[i].Id)))
                            .append($("<td>").text(++counter))
                            .append($("<td>").addClass("pronounce").append($("<span>").text("$$$")))                            
                            .append($("<td>").addClass("word").text(words[i].Word))
                            .append($("<td>").text(words[i].Transcription))
                            .append(
                                $("<td>").append($("<div>").text(words[i].Translation))
                            )
                    );
                }
            }
        });
    }
    getWords();
    filterControl.change(function () {
        getWords(true);
    });
    function fileExists(filename) {

        var response = jQuery.ajax({
            url: filename,
            type: 'HEAD',
            async: false
        }).status;

        return (response != "200") ? false : true;
    }

    function pronunce(word) {
        var fileSrc = "/Content/pronunciation/OtdRealPeopleTTS/" + word[0] + "/" + word + ".mp3";
        if (fileExists(fileSrc))
            player.src = fileSrc;
        else
            player.src = "/Content/pronunciation/WyabdcRealPeopleTTS/" + word[0] + "/" + word + ".wav";
        player.play();   
    }

    $(".english-words").click(function (event) {
        var target = $(event.target);
        var td = target.closest("td");  
        var tr = td.closest("tr");
        if (td.is(".pronounce")) {
            var word = tr.find(".word").text().trim();
            pronunce(word);   
            return;
        }   
        if (td.find("input").length > 0) {
            tr.removeClass("active");
            return;
        }

        var activeTr = $("tr.active");
        if (activeTr.get(0) !== tr.get(0))
            activeTr.removeClass("active");

        tr.toggleClass("active");
    });

    $("#english-words-action-btn").click(function () {
        if (actionInProgress)
            return;
        actionInProgress = true;

        var action = parseInt(actionControl.val());   

        var checkboxs = $("#english-words-tbody input:checked");     
        var wordIds = checkboxs.get().map(function (item) {
            return parseInt(item.id);
        });

        $.ajax({
            url: "api/englishwords",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                action: action,
                wordIds: wordIds
            }),
            success: function () {
                var filter = parseInt(filterControl.val());
                if (filter === 3) {
                    checkboxs.each(function () {
                        this.checked = false;
                    });
                    return;
                }

                if (action !== filter) {
                    checkboxs.each(function () {
                        $(this).closest("tr").remove();
                    });
                }
            },
            complete: function () {
                actionInProgress = false;
            }
        });
    });

    $(window).scroll(function () {
        if (noMore)
            return;

        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            getWords();
        }
    });
});
