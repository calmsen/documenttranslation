﻿$(function () {
    var uploadFileInput = $('#upload-file-input');

    uploadFileInput.on("change", function () {
        var formData = new FormData();
        formData.append('file', uploadFileInput[0].files[0]);

        $.ajax({
            url: '/api/bookparser',
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data) {
                location.reload();
            }
        });
    });
    
});
